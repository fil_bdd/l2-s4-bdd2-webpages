<!-- Le nom de l'UE -->
<div class="UE">Bases de données 2 - BDD2</div>
<div class="bcc">BCC 5 : organiser et gérer des données</div>

<h3> Pré-requis </h3>
Les pré-requis pour cette UE&#xA0;:
<ul>
<li><a href="https://www.fil.univ-lille1.fr/portail/index.php?dipl=L&sem=S3&ue=BDD1&label=L2">UE BDD1</a></li>
</ul>

<!-- Volume horaire et organisation de l'UE C, CTD, TD, TP -->
<h3> Organisation  </h3>

<p>
Cette unité se déroule au S4 de la Licence Informatique. Il s'agit d'une UE obligatoire de cette mention.
</p>
<p>
Volume horaire : 1h30 de cours, 1h30 de TD et 1h30 de TP par semaine, pendant 6 semaines.</p>

<!--p>
Cette UE constitue un pré-requis  de l'<a href="http://portail.fil.univ-lille1.fr/????????">UE XXX</a>.
</p-->



<h3> Crédits </h3>

<strong>3 ECTS</strong>


<!-- le ou les responsable(s) -->
<h3> Responsables </h3>

<p>
<a href="mailto:Marius.BILASCO@univ-lillePOINTfr?subject[BDD2]">Marius BILASCO</a> et 
<a href="mailto:Maude.PUPIN@univ-lillePOINTfr?subject[BDD2]">Maude PUPIN</a>
</p>


<?php
  include("https://gitlab.univ-lille.fr/fil_bdd/l2-s4-bdd2-webpages/-/raw/master/signature.php");
?>   
