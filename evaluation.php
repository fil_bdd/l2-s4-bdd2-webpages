<!-- la classe NOTE attribue une couleur -->
<!-- la classe FORMULE pour les tags  -->

<h3> Calcul de la note </h3>

<p>
L'évaluation s'effectue suivant une procédure de contrôle continu. 

Trois notes seront attribuées à chaque étudiant durant le semestre :
</p>

<ul>
<li> <span class="NOTE">TD</span> : une note sur 20 de TD
 attribuée par l'enseignant de votre groupe 
 (rendus réguliers et conformes aux consignes, présence, rendus notés).  </li>
<li> <span class="NOTE">Interro</span> : une note sur 20 d'une interrogation
écrite de 1h réalisée pendant la 4ème séance de cours.  </li>
<li> <span class="NOTE">DS</span> : une note sur 20 d'un devoir
surveillé en fin de semestre.</li> 
</ul>

<p>
La note finale sur 20 (<span class="NOTE">N</span>) est calculée comme
une moyenne pondérée de ces notes : 
</p>

<p class="FORMULE">
 N = 51% * DSf + 34% * DSi + 15% * TD
</p>


<p>
Une seconde chance est accordée via un calcul différent de la note finale.
Cette note ne peut dépasser 10.
</p>

<p class="FORMULE">
N = 85% * DSf + 15% * TD
</p>
 
<p>
L'unité acquise apporte 3 ECTS.
</p>

<h3> Sujets des années précédentes </h3>

<ul>
    <li> 2023-2024 : <a href="https://www.fil.univ-lille.fr/~maude.pupin/L2/BDD2/DSi_BDD2_2023-2024.pdf">DSi</a>, <a href="https://www.fil.univ-lille.fr/~maude.pupin/L2/BDD2/DSf_BDD2_2023-2024.pdf">DSf</a> </li> 

    <li> 2022-2023 : <a href="https://www.fil.univ-lille.fr/~maude.pupin/L2/BDD2/DSi_BDD2_2022-2023.pdf">DSi</a>, <a href="https://www.fil.univ-lille.fr/~maude.pupin/L2/BDD2/DSf_BDD2_2022-2023.pdf">DSf</a> </li> 

    <li> 2021-2022 : <a href="https://www.fil.univ-lille.fr/~maude.pupin/L2/BDD2/DSi_BDD2_2021-2022.pdf">DSi</a>, <a href="https://www.fil.univ-lille.fr/~maude.pupin/L2/BDD2/DSf_BDD2_2021-2022.pdf">DSf</a> </li> 

    <li> 2020-2021 : <a href="https://www.fil.univ-lille.fr/~maude.pupin/L2/BDD2/DSi_BDD2_2020-2021.pdf">DSi</a>, <a href="https://www.fil.univ-lille.fr/~maude.pupin/L2/BDD2/DSf_BDD2_2020-2021.pdf">DSf</a> </li> 

</ul>


<?php
  include("https://gitlab.univ-lille.fr/fil_bdd/l2-s4-bdd2-webpages/-/raw/master/signature.php");
?>   


