<!-- objectifs de l'UE -->
<h3> Objectifs </h3>

Découvrir la conception d'un schéma de base de données relationnelle,
en commençant par définir le modèle conceptuel de données, puis
en le transformant en modèle logique pour finir par les commandes
SQL de création de tables, y compris avec les contraintes d'intégrité.
Approfondir la création de vue et leur utilisation dans des requêtes complexes.


<!-- le contenu -->
<h3> Contenu </h3>

<ul>
  <li>Modèle conceptuel de donnée (MCD)</li>
  <li>Règles de transformations : MCD -> MLD</li>
  <li>De-normalisation et dépendances fonctionnelles</li>
  <li>Gestion avancée de contraintes d’intégrité</li>
  <li>Création de vues et requêtes complexes</li>
</ul>

<!-- bibliographie accompagnant l'UE -->
<h3> Bibliographie </h3>

<?php
  include("https://gitlab.univ-lille.fr/fil_bdd/l2-s4-bdd2-webpages/-/raw/master/signature.php");
?>   
