<div class="edt">     


<table class="table">
<tr class="entete">
<th> Gpe </th>
<th> Nature </th>
<th> Horaire </th>
<th> Salle </th>
<th> Enseignant </th>
<th> e-mail </th>
</tr>

<!-- ------------------------ EXEMPLE --------------------- -->
<!-- <tr class="TYPE" > avec TYPE = Cours | CTD | TD | TP   -->
<!-- <td> 2</td>					    -->
<!-- <td> td </td>					    -->
<!-- <td> Mercredi 15h45-16h45</td>			    -->
<!-- <td> M5 - A3</td>					    -->
<!-- <td> Machin Chose</td>				    -->
<!-- <td>						    -->
<!-- <a href="mailto:Machin.Chose@univ-lille.fr?subject=[MonUE]"> -->
<!-- Machin.Chose@univ-lille.fr </a>				    -->
<!-- </td>				                    -->
<!-- </tr>                                                  -->
<!-- --------------------- FIN EXEMPLE -------------------- -->

<!-- COURS -->
<tr class="cours" >                        
<td> <!-- GROUPE  -->   Tous        </td>
<td> <!-- NATURE  -->   Cours       </td>
<td> <!-- HORAIRE -->   mardi 10h15-11h45 </td>
<td> <!-- SALLE   -->    </td>
<td> <!-- ENSEIGNANT --> Marius Bilasco ou Maude Pupin </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->

<!-- ============================================================ -->
<!-- TEMPLATE A COPIER -->
<!-- ============================================================ -->
<!-- TD -->
<tr class="TD" >                        
<td> <!-- GROUPE  -->    1      </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->    lundi 13h15-14h45 </td>
<td> <!-- SALLE   -->    M5- </td>
<td> <!-- ENSEIGNANT --> Emilie Allart </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->    1      </td>
<td> <!-- NATURE  -->    TP     </td>
<td> <!-- HORAIRE -->    lundi 15h-16h30 </td>
<td> <!-- SALLE   -->    M5-A11  </td>
<td> <!-- ENSEIGNANT --> Emilie Allart </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TD -->
</tr><tr class="TD" >
<td> <!-- GROUPE  -->    2      </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->    lundi 13h15-14h45 </td>
<td> <!-- SALLE   -->    M5-A </td>
<td> <!-- ENSEIGNANT --> Marius Bilasco </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->    2      </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    mardi 15h-16h30 </td>
<td> <!-- SALLE   -->    M5-A13 </td>
<td> <!-- ENSEIGNANT --> Marius Bilasco </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TD -->
</tr><tr class="TD" >
<td> <!-- GROUPE  -->    3      </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->    mardi 13h15-14h45 </td>
<td> <!-- SALLE   -->    M5- </td>
<td> <!-- ENSEIGNANT --> Jean-François Roos </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->    3      </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    mardi 15h-16h30 </td>
<td> <!-- SALLE   -->    M5-A12 </td>
<td> <!-- ENSEIGNANT --> Jean-François Roos </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TD -->
</tr><tr class="TD" >
<td> <!-- GROUPE  -->    4      </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->    lundi 13h15-14h45 </td>
<td> <!-- SALLE   -->    M5- </td>
<td> <!-- ENSEIGNANT --> Héla Kadri </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->    4      </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    lundi 15h-16h30 </td>
<td> <!-- SALLE   -->    M5-A12 </td>
<td> <!-- ENSEIGNANT --> Héla Kadri </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TD -->
</tr><tr class="TD" >
<td> <!-- GROUPE  -->    5      </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->    vendredi 8h30-10h </td>
<td> <!-- SALLE   -->    M5- </td>
<td> <!-- ENSEIGNANT --> Jean-Stéhpane Varré </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->    5      </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    vendredi 10h15-11h45 </td>
<td> <!-- SALLE   -->    M5-A15 </td>
<td> <!-- ENSEIGNANT --> Jean-Stéhpane Varré </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TD -->
</tr><tr class="TD" >
<td> <!-- GROUPE  -->    6      </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->    lundi 8h30-10h </td>
<td> <!-- SALLE   -->    M5- </td>
<td> <!-- ENSEIGNANT --> Héla Kadri </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->    6      </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    lundi 10h15-11h45 </td>
<td> <!-- SALLE   -->    M5-A12 </td>
<td> <!-- ENSEIGNANT --> Héla Kadri </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TD -->
</tr><tr class="TD" >
<td> <!-- GROUPE  -->    7      </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->    mercredi 13h15-14h45 </td>
<td> <!-- SALLE   -->    M5- </td>
<td> <!-- ENSEIGNANT --> Émilie Allart </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->    7      </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    mercredi 15h-16h30  </td>
<td> <!-- SALLE   -->    M5-A13 </td>
<td> <!-- ENSEIGNANT --> Émilie Allart </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>

<!-- ============================================================ -->
<!-- TD -->
</tr><tr class="TD" >
<td> <!-- GROUPE  -->    IM      </td>
<td> <!-- NATURE  -->    TD      </td>
<td> <!-- HORAIRE -->    vendredi 13h15-14h45 </td>
<td> <!-- SALLE   -->    M5-A8 </td>
<td> <!-- ENSEIGNANT --> Maude Pupin </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>
<!-- ============================================================ -->
<!-- TP -->
</tr><tr class="TP" >                        
<td> <!-- GROUPE  -->    IM      </td>
<td> <!-- NATURE  -->    TP      </td>
<td> <!-- HORAIRE -->    vendredi 15h-16h30 </td>
<td> <!-- SALLE   -->    M5-A4 </td>
<td> <!-- ENSEIGNANT --> Maude Pupin </td>
<td> <!-- MAIL -->       
<!-- <a href="mailto:@univ-lille.fr?subject=[ ]"> @univ-lille.fr</a> -->
</td>
</tr>

<!-- ============================================================ -->
<!-- FIN TEMPLATE -->
<!-- ============================================================ -->

</table>

</div><!-- edt -->

<?php
  include("https://gitlab.univ-lille.fr/fil_bdd/l2-s4-bdd2-webpages/-/raw/master/signature.php");
?>   
