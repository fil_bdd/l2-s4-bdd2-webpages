<div class="semainier">        


<table class="table">

<tr class="entete">
<th>  </th>
<th> Cours </th>
<th> TD </th>
<th> TP </th>
<th> Remarque </th>
</tr>


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 04/01 au 09/01</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td > 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 11/01 au 16/01</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td > 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 18/01 au 23/01</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 25/01 au 30/01</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->
Pas de TD cette semaine
</td>
<td> 
<!-- TP -->
Pas de TP cette semaine
</td>
<td class="remarque"> 
<!-- REMARQUE -->
Début des enseignements de BDD2
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 01/02 au 06/02</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<tr class="seance" >
<!-- FIN SEANCE -->
<td>du 08/02 au 13/02</td>
<td>
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- FIN SEANCE -->
<tr class="seance" >
<td>du 15/02 au 20/02</td>
<td>
<!-- COURS -->

</td>
<td>
<!-- TD -->

</td>
<td>
<!-- TP -->

</td>
<td class="remarque"> 

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<tr class="libre" >
<td>du 22/02 au 27/02</td>
<td>
</td>
<td> 
</td>
<td>
</td>
<td class="remarque">
<!-- REMARQUE -->
interruption pédagogique hiver
</td>
</tr>
<!-- ========================================================= -->





<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 01/03 au 06/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 08/03 au 13/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td>
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->

</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 15/03 au 20/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
Fin des enseignements de BDD2
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 22/03 au 27/03</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<tr class="seance" >
<td>du 29/03 au 03/04</td>
<td> <!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->
</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 05/04 au 10/04</td>
<td> 
</td>
<td> 
</td>
<td> </td>
<td class="remarque"> 
<!-- REMARQUE -->
lundi 5 avril férié
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 12/04 au 17/04</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<!-- SEANCE -->
<tr class="seance" >
<td>du 19/04 au 24/04</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE -->
<!-- ========================================================= -->



<!-- ========================================================= -->
<tr class="libre" >
<td>du 26/04 au 08/05</td>
<td> </td>
<td> </td>
<td> </td>
<td class="remarque"> 
<!-- REMARQUE -->
interruption pédagogique de printemps
</td>
</tr>
<!-- ========================================================= -->




<!-- ========================================================= -->
<!-- SEANCE  -->
<tr class="seance" >
<td>du 10/05 au 15/05</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
jeudi 13 mai férié
</td>
</tr>
	<!-- FIN SEANCE  -->
<!-- ========================================================= -->


<!-- ========================================================= -->
<!-- SEANCE  -->
<tr class="seance" >
<td>du 16/05 au 22/05</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
</td>
</tr>
<!-- FIN SEANCE  -->
<!-- ========================================================= -->

<!-- ========================================================= -->
<!-- SEANCE  -->
<tr class="seance" >
<td>du 24/05 au 29/05</td>
<td> 
<!-- COURS -->

</td>
<td> 
<!-- TD -->

</td>
<td> 
<!-- TP -->

</td>
<td class="remarque"> 
<!-- REMARQUE -->
lundi 24 mai férié
</td>
</tr>
<!-- FIN SEANCE  -->
<!-- ========================================================= -->


</table>



</div>

<?php
  include 'signature.php';
?>   

